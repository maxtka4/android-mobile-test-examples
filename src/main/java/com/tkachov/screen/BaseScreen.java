package com.tkachov.screen;

import com.tkachov.enums.Direction;
import lombok.Getter;
import lombok.experimental.Accessors;
import net.serenitybdd.core.pages.PageObject;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Interaction;

@Accessors(fluent = true)
@Getter
public class BaseScreen extends PageObject {

    public static final int DEFAULT_TRIES = 5;
    private final By popupMessage = By.id("android:id/message");
    private final By frame = By.xpath("//android.widget.FrameLayout");

    public void swipe(Direction direction) {
        Dimension size = getDriver().manage().window().getSize();
        int yOffset = size.getHeight() / 4;
        int xOffset = size.getWidth() / 2;
        switch (direction) {
            case UP:
                xOffset = 0;
                break;
            case DOWN:
                yOffset = -yOffset;
                xOffset = 0;
                break;
            case LEFT:
                yOffset = 0;
                xOffset = -xOffset;
                break;
            case RIGHT:
                yOffset = 0;
        }
        withAction().dragAndDropBy($(frame), xOffset, yOffset).perform();
    }

    //As WebElement.doubleClick() doesn't work in this app, custom method has been created
    public void doubleTap(WebElement elementToTap) {
        Actions actions = withAction();
        Interaction down = actions.getActivePointer().createPointerDown(0);
        Interaction up = actions.getActivePointer().createPointerUp(0);
        actions.moveToElement(elementToTap)
                .tick(down).tick(up)
                .pause(200)
                .tick(down).tick(up)
                .perform();
    }

    public void assertMessage(String actual, String expected) {
        Assertions.assertThat(actual)
                .withFailMessage("%s message should be displayed, but was %s", expected, actual)
                .isEqualTo(expected);
    }

}
