package com.tkachov.screen;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;

@Accessors(fluent = true)
@Getter
public class Header extends BaseScreen {

    private final By backButton = By.xpath("//android.widget.TextView[@text='Back']");
    private final By headerName =
            By.xpath("//android.widget.TextView[@text='Back']/../../../" +
                    "android.view.View/android.widget.TextView[@text!='Samples List']");
}
