package com.tkachov.screen;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;

@Accessors(fluent = true)
@Getter
public class DragAndDropScreen extends BaseScreen {

    private final By rectangle = By.xpath("//android.widget.TextView[@text='Drop here.']");
    private final By circle = By.xpath("//android.widget.TextView[@text='Drag me!']");
    private final By message = By.xpath("//android.widget.TextView[@text='Circle dropped']");
}
