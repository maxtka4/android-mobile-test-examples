package com.tkachov.screen;

import org.openqa.selenium.By;

import java.util.List;

public class NativeViewScreen extends BaseScreen {

   private final By helloWorldItem = By.xpath("//android.widget.TextView[contains(@text, 'Hello World')]");

   public List<String> getHelloWorldItems() {
       return $$(helloWorldItem).texts();
   }
}
