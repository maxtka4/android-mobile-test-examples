package com.tkachov.screen;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;

@Accessors(fluent = true)
@Getter
public class CarouselScreen extends BaseScreen {

    private final By item = By.xpath("//android.widget.HorizontalScrollView//android.widget.TextView");

}
