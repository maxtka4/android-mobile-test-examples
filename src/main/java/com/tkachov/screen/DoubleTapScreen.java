package com.tkachov.screen;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;

@Accessors(fluent = true)
@Getter
public class DoubleTapScreen extends BaseScreen {

    private final By tapMeButton = By.xpath("//android.view.View[@content-desc='doubleTapMe']");

    public void doubleTapOnTapMeButton() {
        doubleTap($(tapMeButton));
    }
}
