package com.tkachov.screen;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;

@Accessors(fluent = true)
@Getter
public class LoginScreen extends BaseScreen {

    private final By usernameInput = By.xpath("//android.widget.EditText[@content-desc='username']");
    private final By passwordInput = By.xpath("//android.widget.EditText[@content-desc='password']");
    private final By submitButton = By.xpath("//android.widget.TextView[@text='LOG IN']");
}
