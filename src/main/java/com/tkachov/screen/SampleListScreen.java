package com.tkachov.screen;

import com.tkachov.enums.Direction;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;

import java.util.List;

@Accessors(fluent = true)
@Getter
public class SampleListScreen extends BaseScreen {

    private final By header = By.xpath("//android.widget.TextView[@text='Samples List']");
    private final By samplesList = By.xpath("//android.widget.TextView[@content-desc]");
    private final String sampleByName = "//android.widget.TextView[@text='{0}']";


    public List<String> getDisplayedSamplesOnTheScreen() {
        return $$(samplesList).texts();
    }

    public void swipeUntilSampleVisible(String sampleName) {
        for (int i = 0; i < DEFAULT_TRIES
                && !getDisplayedSamplesOnTheScreen().contains(sampleName); i++) {
            swipe(Direction.DOWN);
        }
    }
}
