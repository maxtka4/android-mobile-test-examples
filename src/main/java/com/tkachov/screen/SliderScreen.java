package com.tkachov.screen;

import lombok.Getter;
import lombok.experimental.Accessors;
import net.datafaker.Faker;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@Accessors(fluent = true)
@Getter
public class SliderScreen extends BaseScreen {

    private final By slider = By.xpath("//android.widget.SeekBar/../android.widget.TextView");
    private final By sliderLine = By.xpath("//android.widget.SeekBar");

    public void slideToRandomPosition() {
        WebElementFacade line = $(sliderLine);
        int width = line.getRect().getWidth()/2;
        int offset = new Faker().number().numberBetween(-width, width);
        withAction().dragAndDropBy(line, offset, 0).perform();
    }
}
