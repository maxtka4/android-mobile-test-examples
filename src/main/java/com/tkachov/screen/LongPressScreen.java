package com.tkachov.screen;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;

@Accessors(fluent = true)
@Getter
public class LongPressScreen extends BaseScreen {

    private final By longPressMeButton = By.xpath("//android.view.View[@content-desc='longpress']");
    private final By screenSummary = By.xpath("//android.widget.TextView[following-sibling::android.view.View[@content-desc='longpress']]");

}
