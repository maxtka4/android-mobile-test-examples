package com.tkachov.screen;

import com.tkachov.enums.Direction;
import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.tkachov.enums.SessionVariable.TECH_STACK_ITEMS;

public class VerticalSwipingScreen extends BaseScreen {

    private final By techStackItems = By.xpath("//android.widget.ScrollView//android.widget.TextView");

    public void swipeUntilContentVisible(List<String> expectedContent, Direction direction) {
        Set<String> temp = new HashSet<>();
        for (int i = 0; i < DEFAULT_TRIES; i++) {
            List<String> techStackItems = getTechStackItems();
            for (String expected : expectedContent) {
                if (techStackItems.contains(expected)) {
                    temp.add(expected);
                }
            }
            if(temp.size() == expectedContent.size()) {
                Serenity.setSessionVariable(TECH_STACK_ITEMS).to(new ArrayList<>(temp));
                break;
            } else {
                swipe(direction);
            }
        }
    }

    private List<String> getTechStackItems() {
        return $$(techStackItems).texts().stream()
                .map(String::trim).collect(Collectors.toList());
    }
}
