package com.tkachov.stepdefs;

import com.tkachov.screen.LoginScreen;
import com.tkachov.screen.SampleListScreen;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

public class SampleListSteps extends UIInteractionSteps {

    @Steps
    SampleListScreen sampleListScreen;
    LoginScreen loginScreen;

    @When("I navigate to {string} screen")
    public void navigateToScreen(String screen) {
        sampleListScreen.swipeUntilSampleVisible(screen);
        clickOn($(sampleListScreen.sampleByName(), screen));
    }

    @Then("I should see samples list")
    public void verifySampleList() {
        Assertions.assertThat(isElementVisible(sampleListScreen.header()))
                .withFailMessage("Samples List screen should be displayed")
                .isTrue();
        Assertions.assertThat(sampleListScreen.getDisplayedSamplesOnTheScreen().size())
                .withFailMessage("At lease one sample in the list should be displayed")
                .isGreaterThan(0);
    }

    @Then("I should see error {string} message")
    public void verifyErrorMessage(String expectedMessage) {
        String actualMessage = textOf(loginScreen.popupMessage());
        sampleListScreen.assertMessage(actualMessage, expectedMessage);
    }
}
