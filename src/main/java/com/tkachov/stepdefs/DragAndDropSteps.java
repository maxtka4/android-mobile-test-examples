package com.tkachov.stepdefs;

import com.tkachov.screen.DragAndDropScreen;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class DragAndDropSteps extends UIInteractionSteps {

    @Steps
    DragAndDropScreen dragAndDropScreen;

    @When("I drag and drop circle in rectangle")
    public void dragAndDropCircleInRectangle () {
        withAction().dragAndDrop($(dragAndDropScreen.circle()),
                $(dragAndDropScreen.rectangle())).perform();
    }

    @Then("I should see {string} message")
    public void verifyMessageDisplayed(String expectedMessage) {
        String actualMessage = textOf(dragAndDropScreen.message());
        dragAndDropScreen.assertMessage(actualMessage, expectedMessage);
    }
}
