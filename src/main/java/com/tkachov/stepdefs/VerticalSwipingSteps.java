package com.tkachov.stepdefs;

import com.tkachov.enums.Direction;
import com.tkachov.screen.VerticalSwipingScreen;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

import java.util.List;

import static com.tkachov.enums.SessionVariable.TECH_STACK_ITEMS;


public class VerticalSwipingSteps extends UIInteractionSteps {

    @Steps
    VerticalSwipingScreen verticalSwipingScreen;

    @Then("I should see following in list:")
    public void verifyContentInList(List<String> expectedToBeInList) {
        verticalSwipingScreen.swipeUntilContentVisible(expectedToBeInList, Direction.DOWN);
        List<String> actual = Serenity.sessionVariableCalled(TECH_STACK_ITEMS);
        Assertions.assertThat(actual)
                .withFailMessage("%s item should be displayed in list, but was %s", expectedToBeInList, actual)
                .isEqualTo(expectedToBeInList);
    }
}
