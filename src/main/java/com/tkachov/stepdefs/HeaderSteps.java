package com.tkachov.stepdefs;

import com.tkachov.screen.Header;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class HeaderSteps extends UIInteractionSteps {

    @Steps
    Header header;

    @Then("I should see {string} header")
    public void verifyHeaderName(String name) {
        header.assertMessage(textOf(header.headerName()), name);
    }
}
