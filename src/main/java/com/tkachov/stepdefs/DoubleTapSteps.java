package com.tkachov.stepdefs;

import com.tkachov.screen.DoubleTapScreen;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class DoubleTapSteps extends UIInteractionSteps {

    @Steps
    DoubleTapScreen doubleTapScreen;

    @When("I double tap on button")
    public void doubleTapOnButton() {
        doubleTapScreen.doubleTapOnTapMeButton();
    }

    @Then("I should see double tap popup with {string} message")
    public void verifyMessageInPopup(String expectedMessage) {
        String actualMessage = textOf(doubleTapScreen.popupMessage());
        doubleTapScreen.assertMessage(actualMessage, expectedMessage);
    }
}
