package com.tkachov.stepdefs;

import com.tkachov.screen.NativeViewScreen;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

public class NativeViewSteps extends UIInteractionSteps {

    @Steps
    NativeViewScreen nativeViewScreen;

    @Then("I should see {int} hello world messages")
    public void verifyHelloWorldMessagesCount(int count) {
        Assertions.assertThat(nativeViewScreen.getHelloWorldItems().size())
                .withFailMessage("%s items should be displayed", count)
                .isEqualTo(count);
    }
}
