package com.tkachov.stepdefs;

import com.tkachov.screen.SliderScreen;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

import static com.tkachov.enums.SessionVariable.DEFAULT_SLIDER_POSITION;


public class SliderSteps extends UIInteractionSteps {

    @Steps
    SliderScreen sliderScreen;

    @When("I click in the middle of slider")
    public void clickOnSlider() {
        Serenity.setSessionVariable(DEFAULT_SLIDER_POSITION).to(textOf(sliderScreen.slider()));
        clickOn($(sliderScreen.sliderLine()));
    }

    @When("I move slider")
    public void moveSlider() {
        Serenity.setSessionVariable(DEFAULT_SLIDER_POSITION).to(textOf(sliderScreen.slider()));
        sliderScreen.slideToRandomPosition();
    }

    @Then("slider position should be changed")
    public void verifySliderPosition() {
        String actualSliderPosition = textOf(sliderScreen.slider());
        Assertions.assertThat(actualSliderPosition)
                .withFailMessage("Slider should change position")
                .isNotEqualTo(Serenity.sessionVariableCalled(DEFAULT_SLIDER_POSITION));
    }
}
