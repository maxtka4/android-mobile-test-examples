package com.tkachov.stepdefs;

import com.tkachov.configuration.User;
import com.tkachov.screen.LoginScreen;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

import static com.tkachov.enums.SessionVariable.CURRENT_USER;


public class LoginSteps extends UIInteractionSteps {

    @Steps
    LoginScreen loginScreen;

    @Given("I have {string} credentials")
    public void putUserToSession(String user) {
        User userCredentials = new User(user);
        Serenity.setSessionVariable(CURRENT_USER).to(userCredentials);
    }

    @Given("I log in to VodQa")
    public void login() {
        clickOn($(loginScreen.submitButton()));
    }

    @When("I log in to the app")
    @When("I try to log in to the app")
    public void loginAs() {
        User user = Serenity.sessionVariableCalled(CURRENT_USER);
        typeInto($(loginScreen.usernameInput()), user.getUserName());
        typeInto($(loginScreen.passwordInput()), user.getPassword());
        clickOn($(loginScreen.submitButton()));
    }
}
