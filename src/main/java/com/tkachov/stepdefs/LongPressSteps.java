package com.tkachov.stepdefs;

import com.tkachov.screen.LongPressScreen;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class LongPressSteps extends UIInteractionSteps {

    @Steps
    LongPressScreen longPressScreen;

    @When("I long press on button")
    public void longPressOnButton() {
        withAction().clickAndHold($(longPressScreen.longPressMeButton())).perform();
    }

    @Then("I should see long presses popup with {string} message")
    public void verifyMessageInPopup(String expectedMessage) {
        String actualMessage = $(longPressScreen.popupMessage()).waitUntilPresent().getText();
        longPressScreen.assertMessage(actualMessage, expectedMessage);
    }

    @Then("I should see summary with {string} message")
    public void iShouldSeeSummaryWithMessage(String summary) {
        String actual = textOf(longPressScreen.screenSummary());
        longPressScreen.assertMessage(actual, summary);
    }
}
