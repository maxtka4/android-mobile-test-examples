package com.tkachov.stepdefs;

import com.tkachov.enums.Direction;
import com.tkachov.screen.CarouselScreen;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class CarouselSteps extends UIInteractionSteps {

    @Steps
    CarouselScreen carouselScreen;

    @When("I swipe {direction}")
    public void swipeInDirection(Direction direction) {
        carouselScreen.swipe(direction);
    }

    @Then("I should see item {string}")
    public void verifyItemNumber(String item) {
        String actual = textOf(carouselScreen.item());
        carouselScreen.assertMessage(actual, item);
    }

    @ParameterType("LEFT|RIGHT|UP|DOWN")
    public Direction direction(String direction) {
        return Direction.valueOf(direction);
    }
}
