package com.tkachov.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
