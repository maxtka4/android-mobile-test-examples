package com.tkachov.enums;

public enum SessionVariable {
    CURRENT_USER,
    DEFAULT_SLIDER_POSITION,
    TECH_STACK_ITEMS
}
