package com.tkachov.configuration;

import com.typesafe.config.Config;
import lombok.Data;
import net.serenitybdd.core.Serenity;

@Data
public class User {
    private String userName;
    private String password;

    public User(String user) {
        Config userConfig = Serenity.environmentVariables()
                .getConfig("users").getConfig(user);
        userName = userConfig.getString("userName");
        password = userConfig.getString("password");
    }
}
