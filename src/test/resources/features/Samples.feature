@VodQa
Feature: Demonstrate actions that can be executed in each sample

  @003
  Scenario: Verify Native View screen content
    Given I log in to VodQa
    When I navigate to "Native View" screen
    Then I should see "Native View Demo" header
    And I should see 3 hello world messages

  @004
  Scenario: Verify changing slider position by click on Slider screen
    Given I log in to VodQa
    When I navigate to "Slider" screen
    Then I should see "Slider" header
    When I click in the middle of slider
    Then slider position should be changed

  @005
  Scenario: Verify changing slider position by move on Slider screen
    Given I log in to VodQa
    When I navigate to "Slider" screen
    When I move slider
    Then slider position should be changed

  @006
  Scenario: Verify swiping on Vertical swiping screen
    Given I log in to VodQa
    When I navigate to "Vertical swiping" screen
    Then I should see "Vertical swiping" header
    And I should see following in list:
      | Java   |
      | Appium |

  @007
  Scenario: Verify drag and drop on Drag and Drop screen
    Given I log in to VodQa
    When I navigate to "Drag & Drop" screen
    Then I should see "Drag & Drop" header
    When I drag and drop circle in rectangle
    Then I should see "Circle dropped" message

  @008
  Scenario: Verify double tap on Double Tap screen
    Given I log in to VodQa
    When I navigate to "Double Tap" screen
    Then I should see "Double Tap Demo" header
    When I double tap on button
    Then I should see double tap popup with "Double tap successful!" message

  @009
  Scenario: Verify long press on Long Press screen
    Given I log in to VodQa
    When I navigate to "Long Press" screen
    Then I should see "Long Press Demo" header
    When I long press on button
    Then I should see long presses popup with "you pressed me hard :P" message

  @010
  Scenario: Verify summary of Long Press screen
    Given I log in to VodQa
    When I navigate to "Long Press" screen
    Then I should see summary with "Long press the Button" message

  @011
  Scenario: Verify swiping on Carousel screen
    Given I log in to VodQa
    When I navigate to "Carousel" screen
    Then I should see "Carousel - Swipe left/right" header
    When I swipe LEFT
    Then I should see item "2"
    When I swipe RIGHT
    Then I should see item "1"
    When I swipe RIGHT
    Then I should see item "3"
