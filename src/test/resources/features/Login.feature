@VodQa
Feature:Login to VodQa application

  @001
  Scenario: Verify success login to VodQa app
    Given I have "admin" credentials
    When I log in to the app
    Then I should see samples list

  @002
  Scenario: Verify invalid credentials message when user doesn't have access to app
    Given I have "wrongAccess" credentials
    When I try to log in to the app
    Then I should see error "Invalid  Credentials" message
