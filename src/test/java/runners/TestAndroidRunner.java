package runners;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = {"classpath:features"},
        glue = {"com.tkachov.stepdefs"}
)
public class TestAndroidRunner {

    static AppiumDriverLocalService service = AppiumDriverLocalService
                    .buildService(new AppiumServiceBuilder()
                    .withArgument(GeneralServerFlag.LOG_LEVEL, "info"));

    static boolean localStartAppium = Boolean.parseBoolean(Serenity.environmentVariables().getProperty("startLocalServer"));

    @BeforeClass
    public static void startAppium() {
        if (localStartAppium) {
            service.start();
        }
    }

    @AfterClass
    public static void stopAppium() {
        if (localStartAppium && service.isRunning()) {
            service.stop();
        }
    }
}
