# Android Automation Testing with Maven and Appium

This guide provides step-by-step instructions on setting up Android automation testing using Maven and Appium on a Mac
machine.

## Prerequisites

1. Java Development Kit (
   JDK): [Download and install JDK](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
2. Android Studio: [Download and install Android Studio](https://developer.android.com/studio)
3. Homebrew Package Manager: [Install Homebrew](https://brew.sh)
4. Node.js and NPM: Install using Homebrew with the following command:
   ```
   brew install node
   ```

## Setup Instructions

1. Clone the repository:
   ```
   git clone https://gitlab.com/maxtka4/android-mobile-test-examples.git
   cd <repository-directory>
   ```

2. Install Maven:
   ```
   brew install maven
   ```

3. Set up Android SDK:
    - Open Android Studio, go to **Preferences** or **Settings**.
    - Navigate to **Appearance & Behavior** > **System Settings** > **Android SDK**.
    - Select the required Android SDK versions and click **Apply** or **OK**.

4. Set up ANDROID_HOME environment variable:
    - Open the Terminal, and execute the following command:
      ```
      echo "export ANDROID_HOME=/Users/YourUsername/Library/Android/sdk" >> ~/.bash_profile
      source ~/.bash_profile
      ```

5. Install Appium:
   ```
   npm install -g appium
   ```

6. Start Appium Server:
    - if you want to start appium separately from test run, open `src/test/resources/serenity.conf` and set
      the `startLocalServer` to false
    - Open the Terminal, and execute the following command

      ```
      appium
      ```

   - if you want to use embedded appium server, open `src/test/resources/serenity.conf` and set the `startLocalServer` to
     true
   - No need to open terminal and start appium, it will be started automatically during test run

7. Configure desired capabilities:
    - Open `src/test/resources/serenity.conf` and update the `appium.capabilities` section as per your device and
      application specifications.

8. Run the tests locally:
   ```
   mvn clean verify -Dtags=@VodQa -Denvironment=local
   ```
9. Run the tests remotely:
   ```
   mvn clean verify -Dtags=@VodQa -Denvironment=remote
   ```
10. Generate serenity report:
   ```
   mvn serenity:aggregate
   ```
11. Review results in gitlab pipeline
    - Open gitlab repository latest pipeline
    - There is a link to report in logs - Serenity Report <link to report>
    - By clicking on the link you can review detailed report with screenshots for failed tests
    - By clicking on the link below Uploading artifacts... you can download artifact, unzip it, open index.html to review report locally
